Induktivni in koinduktivni podatkovni tipi
==========================================

Diplomski seminar pri programu matematika na FMF, 1. stopnja.

mentor: prof. dr. Andrej Bauer

avtor: Jure Slak

Induktivni in koinduktivni podatkovni tipi -- povzetek
------------------------------------------------------

V programskih jezikih se pogosto srečamo z rekurzivnimi podatkovnimi tipi --
tipi, ki se v svoji definiciji sklicujejo sami nase. Poseben primer so
induktivni in koinduktivni tipi, ki jih obravnavamo v tem delu. Najprej si
ogledamo motivacijo in uvod v temo, kjer razvijemo ideje za definicije in iščemo
skupne lastnosti takih tipov. Sledi matematična obravnava induktivnih tipov kot
začetnih algeber in koinduktivnih tipov kot končnih koalgeber v okviru teorije
kategorij. Drugi del naloge se posveti obravnavi osnovnih primerov induktivnih
tipov kot so naravna števila in seznami, medtem ko teorijo koinduktivnih tipov
predstavimo na primeru tokov in konaravnih števil. Na kratko se posvetimo tudi
vprašanju eksistence začetnih algeber in končnih koalgeber. Preostanek dela
namenimo znanima principoma rekurzije in indukcije ter malo manj znanima
principoma korekurzije in koindukcije. Vse te pojme formaliziramo v okviru
algeber in koalgeber za funktor ter si ogledamo primere rekurzivnih in
korekurzivnih definicij funkcij. Za konec podrobneje analiziramo še primere
dokazov z indukcijo in koindukcijo ter s pomočjo pojmov bisimulacije in
kongruence poudarimo njihovo dualnost.

Inductive and Coinductive Data Types -- Abstract
------------------------------------------------

In programming languages, we often work with recursive data types -- types that
refer to themselves in their definitions. We dedicate this work to a notable special
case of inductive and coinductive data types. The first part of the work is meant as an
introduction to the topic observing common properties of such data types and serving
as a motivation for subsequent definitions. A formal mathematical treatment of
inductive data types as initial algebras and coinductive types as final
coalgebras follows, making use of category theory. The second part of the
work presents the treatment of natural numbers and lists as inductive data
types, while the theory of coinductive data types is shown using streams and
conatural numbers. A question of existence of initial algebras and final
coalgebras is also given some attention. The rest of the work focuses on
well-known principles of recursion and induction and less known principles of
corecursion and coinduction. All aforementioned notions are formalised using
algebras and coalgebras for a functor and we give examples of recursive and
corecursive definitions of functions. Finally, we present examples of proofs
using induction and coinduction and analyse them in detail. Their duality is
then further emphasised using the notions of bisimulation and congruence.
