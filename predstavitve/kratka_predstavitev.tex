\documentclass{beamer}

\usepackage[slovene]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[all]{xy}
\usepackage{minted}
\usepackage{lmodern}
\renewcommand\textbullet{\ensuremath{\bullet}}

\newcommand{\N}{\ensuremath{\mathbb{N}}}

\title{Rekurzivni podatkovni tipi}
\author{Jure Slak}
\institute{mentor: prof. dr. Andrej Bauer}

\usetheme{Singapore}
\usecolortheme{crane}

\setbeamercolor{alerted text}{fg=orange}

\newcommand{\kat}{\ensuremath{Kat}}

\newenvironment{displaymath*}{
  \begin{displaymath}
  \xymatrixcolsep{40pt}
  \xymatrixrowsep{40pt}
}{
  \end{displaymath}
}

\begin{document}

\frame{\titlepage}

\section{Motivacija}
\begin{frame}{Motivacija}
  Želimo matematični model algebraičnih podatkovnih tipov.\\[12pt]
  \begin{block}{Nekaj primerov algebraičnih podatkovnih tipov}
    \begin{tabular}[h]{rll}
      $\bullet$ & \texttt{bool}    & $\top$ ali $\bot$ \\
      $\bullet$ & \texttt{pair}    & $(1, 2)$ \\
      $\bullet$ & \texttt{list}    & $a \to g \to r \to y \to \ast$ \\
      $\bullet$ & \texttt{stream}  & $1, 7, -1, 6, 4, \dots$\\
      $\bullet$ & \texttt{nat}     & 42 \\
      $\bullet$ & \texttt{tree}    &
    \end{tabular}
  \vspace{-10pt}

  \hspace{50pt} \includegraphics[width=3cm]{slike/tree.pdf}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Primeri}
  \begin{minted}{Haskell}
    data Nat    = Zero | Succ Nat
    data List a = Nil | Cons a (List a)
    data Tree   = Leaf Int | Node Tree Tree

    data Expression = Number Int
                    | Add Expression Expression
                    | Minus Expression
                    | Mult Expression Expression
                    | Divide Expression Expression

    data Stream a = Cons a (Stream a)
  \end{minted}
\end{frame}

\section{Osnovne definicije}
\begin{frame}{$F$-algebra}
  Naj bo \kat{} distributivna kategorija, t.j. kategorija v kateri privzamemo obstoj končnih
  produktov in koproduktov in distributivnost produktov glede na
  koprodukte.\\[12pt]
  Naj bo $F\colon \kat \to \kat$ endofunktor na kategoriji \kat. \\
  \alert{$F$-algebra} je par $(C, \varphi)$, kjer je $C$ objekt kategorije
  \kat{} in $\varphi\colon F(C) \to C$ morfizem v \kat.
  \vspace{20pt}
  \begin{displaymath*}
    \xymatrix{ F(C) \ar[r]^\varphi & C}
  \end{displaymath*}
\end{frame}

\begin{frame}{Homomorfizmi}
  Naj bosta $(C, \varphi)$ in $(D, \psi)$ $F$-algebri. \\
  \alert{Homomorfizem} $f$ med $(C, \varphi)$ in $(D, \psi)$ je tak morfizem
  $f\colon C \to D$ iz \kat{}, da velja
  \[ f \circ \varphi = \psi \circ F(f), \]
  oz. da komutira naslednji diagram.
  \begin{displaymath*}
    \xymatrix{ F(C) \ar[r]^\varphi \ar[d]_{F(f)} & C \ar[d]^f \\
    F(D) \ar[r]^\psi & D}
  \end{displaymath*}
\end{frame}

\begin{frame}{Kategorija $F$-algeber}
  Naj bo dan endofunktor $F$ na kategoriji \kat. Definiramo kategorijo $F$-algeber nad
  \kat.
  \begin{itemize}
    \item \textbf{Objekti}: $(C, \varphi)$, $\varphi\colon F(C) \to C$.
    \item \textbf{Morfizmi:} Homomorfizmi med $F$-algebrami.
    \item \textbf{Identiteta:} $\text{id}_C$.
    \item \textbf{Kompozitum:} \vspace{-30pt}
      \begin{displaymath*}
        \xymatrix{
          F(C) \ar[r]^\varphi \ar[d]^{F(f)} \ar@/_1.5pc/[dd]_{F(g \circ f)} & C
          \ar[d]^f \ar@/^1.5pc/[dd]^{g\circ f}\\
          F(D) \ar[r]^\psi \ar[d]^{F(g)} & D \ar[d]^g \\
          F(E) \ar[r]^\chi & E
        }
      \end{displaymath*}
  \end{itemize}
\end{frame}

\section{Induktivni tipi}

\begin{frame}{Induktivni tipi}
  Induktivne tipe modeliramo kot \alert{začetne} objekte v kategoriji $F$-algeber.

  \begin{block}{Naravna števila}
    Imejmo dani funkciji $zero\colon 1 \to \N$ in $succ\colon \N \to \N$ s predpisoma
    \begin{align*}
      zero()  &= 0, \\
      succ(n) &= n + 1.
    \end{align*}
    S pomočjo koprodukta ti dve funkciji združimo v eno
    \[ [zero, succ]\colon 1 + \N \to \N \]

    Naravna števila modeliramo kot začetni objekt kategorije $N$-algeber za
    funktor $N(X) = 1 + X$.
  \end{block}
\end{frame}

\begin{frame}{Še več primerov}
  \begin{block}{Induktivni}
    \begin{itemize}
      \item Drevesa: $F(X) = 1 + X \times X$
      \item Seznami: $F(X) = 1 + A \times X$
      \item Prazen tip: $ F(X) = X$
    \end{itemize}
  \end{block}
  \begin{block}{Koinduktivni}
    \begin{itemize}
      \item Stream: $F(X) = A \times X$
      \item Enotski tip: $F(X) = X$
    \end{itemize}
  \end{block}
\end{frame}

\section{Vsebina naloge}
\begin{frame}{Pregled}
  \begin{itemize}
    \setlength{\itemsep}{12pt}
    \item Modeli induktivnih in koinduktivnih tipov
    \item Primeri induktivnih in koinduktivnih tipov
    \item Lambekova lema
    \item Obravnava rekurzije in korekurzije
    \item Ta model ni dober za vse tipe
  \end{itemize}
\end{frame}

\end{document}

% vim: spell spelllang=sl
% vim: foldlevel=99
