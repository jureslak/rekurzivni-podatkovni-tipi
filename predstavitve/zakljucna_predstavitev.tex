\documentclass{beamer}

\usepackage[slovene]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{mathrsfs}
\usepackage[all]{xy}
\usepackage{stmaryrd} % oklepaji
\usepackage{lmodern}
\renewcommand\textbullet{\ensuremath{\bullet}}

% ukazi za matematicna okolja
\theoremstyle{definition} % tekst napisan pokoncno
\newtheorem{definicija}{Definicija}[section]
\newtheorem{primer}[definicija]{Primer}
\newtheorem{opomba}[definicija]{Opomba}

\theoremstyle{plain} % tekst napisan posevno
\newtheorem{lema}[definicija]{Lema}
\newtheorem{izrek}[definicija]{Izrek}
\newtheorem{trditev}[definicija]{Trditev}
\newtheorem{posledica}[definicija]{Posledica}


\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\F}{\ensuremath{F}}
\DeclareMathOperator{\ID}{id}
\newcommand{\id}[1]{{\ensuremath{\ID_{#1}}}}
\newcommand{\inl}{\ensuremath{\iota_1}}
\newcommand{\inr}{\ensuremath{\iota_2}}
\newcommand{\prl}{\ensuremath{p_1}}
\newcommand{\prr}{\ensuremath{p_2}}
\newcommand{\CN}{\N_{\infty}}

% oklepaji
\newcommand{\lbanana}{\llparenthesis}
\newcommand{\rbanana}{\rrparenthesis}
\newcommand{\ro}[1]{\ensuremath{\lbanana#1\rbanana}}
\newcommand{\sq}[1]{\ensuremath{\llbracket#1\rrbracket}}

% funkcije
\newcommand{\func}[1]{\ensuremath{\mathtt{#1}}}
\newcommand{\fold}{\func{fold}}
\newcommand{\zero}{\func{zero}}
\renewcommand{\succ}{\func{succ}}
\newcommand{\pred}{\func{pred}}
\newcommand{\nil}{\func{nil}}
\newcommand{\cons}{\func{cons}}
\newcommand{\head}{\func{head}}
\newcommand{\tail}{\func{tail}}
\newcommand{\len}{\func{length}}
\newcommand{\dbl}{\func{dbl}}
\newcommand{\mergef}{\func{merge}}
\newcommand{\even}{\func{even}}
\newcommand{\odd}{\func{odd}}
\newcommand{\map}{\func{map}}

\newcommand{\fht}{\ensuremath{\langle \head, \tail \rangle}}
\newcommand{\fnc}{\ensuremath{[\nil, \cons]}}

% kategorije
\newcommand{\Alg}[1]{\ensuremath{\mathcal{A}lg(#1)}}
\newcommand{\Coalg}[1]{\ensuremath{\mathcal{C}o\mathcal{A}lg(#1)}}
\newcommand{\Set}{\ensuremath{\mathcal{S}et}}

\title{Induktivni in koinduktivni podatkovni tipi}
\author{Jure Slak}
\institute{mentor: prof.~dr.~Andrej Bauer}
\date{14.~julij~2015}

\usetheme{Singapore}
\usecolortheme{crane}
\usefonttheme[onlymath]{serif}

\setbeamercolor{alerted text}{fg=orange}
\setbeamertemplate{headline}{}

\newcommand{\kat}{\ensuremath{\mathscr{C}}}
\renewcommand{\theta}{\vartheta}

\xymatrixcolsep{40pt}
\xymatrixrowsep{40pt}

\begin{document}

\frame{\titlepage}

\begin{frame}[t]{Rekurzivni podatkovni tipi}
  Rekurzivni tipi so taki, ki se v definiciji sklicujejo sami nase. \\[2ex]

  \begin{columns}
    \begin{column}{0.45\textwidth}
      \alert{\large Seznami} \\[1ex]
      Seznam je bodisi prazen bodisi je sestavljen iz enega elementa in
      \emph{seznama}.
      \[ L \cong 1 + A \times L \]
      Konstruktorja:  \\[-4ex]
      \begin{align*}
        \nil&\colon 1 \to L \\
        \cons&\colon A \times L \to L \\
        \fnc&\colon 1+A\times L \to L \\
        \zeta&\colon \F(L) \to L
      \end{align*}
    \end{column}
    \begin{column}{0.45\textwidth}
      \alert{\large Tokovi} \\[1ex]
      Tok je sestavljen iz elementa in \emph{toka}. \\[2ex]
      \[ S \cong A \times S \]
      Destruktorja: \\[-4ex]
      \begin{align*}
        \head&\colon S \to A \\
        \tail&\colon S \to S \\
        \fht&\colon S \to A \times S \\
        \sigma&\colon S \to \F(S)
      \end{align*}
%       \vfill
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Algebre in koalgebre}
  Naj bo $\F\colon \kat \to \kat$ endofunktor na kategoriji \kat. \\[4ex]
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \alert{$\F$-algebra} je par $(C, \varphi)$, kjer je $C$ objekt kategorije
      \kat{} in $\varphi\colon \F(C) \to C$ morfizem v~\kat.
      \[
        \xymatrix{\F(C) \ar[r]^\varphi & C}
      \]
    \end{column}
    \begin{column}{0.45\textwidth}
      \alert{$\F$-koalgebra} je par $(P, \pi)$, kjer je $P$ objekt kategorije
      \kat{} in $\pi\colon P \to \F(P)$ morfizem v~\kat.
      \[
        \xymatrix{P \ar[r]^\pi & \F(P)}
      \]
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Homomorfizmi}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      Naj bosta $(C, \varphi)$ in $(D, \psi)$ $\F$-algebri. \\[1ex]
      \alert{Homomorfizem} $f$ med $(C, \varphi)$ in $(D, \psi)$ je tak morfizem
      \[ f\colon C \to D \] iz \kat{}, da komutira diagram
      \[
        \xymatrix{ \F(C) \ar[r]^\varphi \ar[d]_{\F(f)} & C \ar[d]^f \\
        \F(D) \ar[r]^\psi & D}
      \]
    \end{column}
    \begin{column}{0.5\textwidth}
      Naj bosta $(P, \pi)$ in $(Q, \theta)$ $\F$-koalgebri. \\[1ex]
      \alert{Homomorfizem} $k$ med $(P, \pi)$ in $(Q, \theta)$ je tak morfizem
      \[ k\colon P \to Q \] iz \kat{}, da komutira diagram
      \[
        \xymatrix{ P \ar[r]^\pi \ar[d]_{k} & \F(P) \ar[d]^{\F(k)} \\
        Q \ar[r]^\theta & \F(Q)}
      \]
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Kategoriji $\F$-algeber in $\F$-koalgeber}
  Naj bo dan endofunktor $\F$ na kategoriji \kat. \\[2ex]
  Definiramo \alert{kategorijo $\F$-algeber} nad \kat.
  \begin{itemize}
    \item \textbf{objekti}: $\F$-algebre $(C, \varphi)$, $\varphi\colon \F(C) \to C$
    \item \textbf{morfizmi:} homomorfizmi med $\F$-algebrami
    \item \textbf{kompozitum:} kot v \kat
    \item \textbf{identiteta:} $\text{id}_C$, kot v \kat
  \end{itemize} \vspace{2ex}
  Definiramo \alert{kategorijo $\F$-koalgeber} nad \kat.
  \begin{itemize}
    \item \textbf{objekti}: $\F$-koalgebre $(P, \pi)$, $\pi \colon P \to \F(P)$
    \item \textbf{morfizmi:} homomorfizmi med $\F$-koalgebrami
    \item \textbf{kompozitum:} kot v \kat
    \item \textbf{identiteta:} $\text{id}_P$, kot v \kat
  \end{itemize}
\end{frame}

\begin{frame}{Začetne algebre in končne koalgebre}
  $\F$-algebra $(M, \mu)$ je \alert{začetna}, če je začetni objekt v kategoriji
  $\F$-algeber, tj.~za vsako $\F$-algebro $(C, \varphi)$ obstaja enoličen morfizem
  \[ \ro{\varphi}\colon (M, \mu) \to (C, \varphi). \]

  \vspace{3ex}

  $\F$-koalgebra $(N, \nu)$ je \alert{končna}, če je končni objekt v kategoriji
  $\F$-koalgeber, tj.~za vsako $\F$-koalgebro $(P, \pi)$ obstaja enoličen morfizem
  \[ \sq{\pi}\colon (P, \pi) \to (N, \nu). \]
\end{frame}

\begin{frame}{Lambekova lema}
  \begin{izrek}[Lambekova lema]
    Naj bo $(M, \mu)$ začetna algebra za funktor $\F$. Potem je $\mu$ izomorfizem v
    $\Set$ z inverzom
    \[ \mu^{-1} = \ro{\F(\mu)}. \]
  \end{izrek}
  \begin{izrek}
    Naj bo $(N, \nu)$ končna koalgebra za funktor $\F$. Potem je $\nu$ izomorfizem v
    \Set{} z  inverzom
    \[  \nu^{-1} = \sq{\F(\nu)}. \]
  \end{izrek}
  \emph{Ideja dokaza: ogledamo si diagram.}
\end{frame}

% \begin{frame}{Dokaz}
%   Oglejmo si diagram.
%   $
%     \xymatrix{
%       \F(M) \ar[r]^\mu \ar[d]_{\F\ro{\F(\mu)}} \ar@{.>} @/_1.1pc/ []!<1ex,-2ex>;[rd]!<-3ex,1ex>^{\id{\F(M)}} &
%         M \ar[d]_{\ro{\F(\mu)}} \ar@/^1.9pc/ @{-->}[dd]^{\ro{\mu} = \id{M}}\\
%       \F(\F(M)) \ar[r]_{\F(\mu)} \ar[d]_{\F(\mu)} & \F(M) \ar[d]_\mu \\
%       \F(M) \ar[r]^\mu & M \\
%     }
%   $
%   \begin{columns}
%     \begin{column}{0.3\textwidth}
%       Vidimo:
%       \[ \quad \mu \circ \ro{\F(\mu)} = \id{M} \]
%       in
%     \end{column}
%     \begin{column}{0.6\textwidth}
%       \begin{align*}
%         \ro{\F(\mu)} \circ \mu &= \F(\mu) \circ \F\ro{\F(\mu)} \\
%         &= \F(\mu \circ \ro{\F(\mu)}) \\
%         &= \F(\id{M})  \\
%       &= \id{\F(M)}.
%     \end{align*}
%     \end{column}
%   \end{columns}
% \end{frame}

\begin{frame}{Eksistenčni izrek}
  \begin{izrek}
    \label{izr:eksis}
    Naj bo $F\colon\Set\to\Set$ polinomski funktor, tj.~oblike
    \[ F(X) = \coprod_{i \in I}A_i\times X^i,\]
    za neko družino množic $(A_i)_{i\in I}$, indeksirano s končno podmnožico
    naravnih števil.  Potem obstajata začetna algebra v kategoriji $F$-algeber
    in končna koalgebra v kategoriji $F$-koalgeber.
  \end{izrek}
  \emph{Ideja dokaza: konstrukcija.}
\end{frame}

\begin{frame}{Naravna in konaravna števila}
  Funktor $F(X) = 1 + X$. \\[4ex]

  \begin{columns}
    \begin{column}{0.45\textwidth}
      \alert{Naravna števila}:
      \[ \N = \{0, 1, 2, \ldots \} \]
      Začetna algebra: $(\N, [\zero, \succ])$
      \begin{align*}
        \zero&\colon 1 \to \N \\ \succ &\colon \N \to \N
      \end{align*}
    \end{column}
    \begin{column}{0.45\textwidth}
      \alert{Konaravna števila}:
      \[ \CN = \N \cup \{ \infty \} \]
      Končna koalgebra: $(\CN, \pred)$
      \begin{align*}
        \pred\colon\CN\to 1+\CN \\
      \end{align*}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Princip rekurzije in korekurzije}
  \setlength{\textwidth}{0.87\paperwidth}
  \begin{definicija}[Princip rekurzije]
    Naj bo $(M, \mu)$ začetna $F$-algebra.
    Za vsako $F$-algebro $(C, \varphi)$
    obstaja enoličen morfizem $\ro{\varphi}\colon M \to C$.\\[2ex]

    Za funkcijo $\ro{\varphi}$ pravimo, da je definirana
    \alert{rekurzivno}.
  \end{definicija} \vspace{1ex}
  \begin{definicija}[Princip korekurzije]
    Naj bo $(N, \nu)$ končna $F$-koalgebra. Za vsako $F$-koalgebro $(P, \pi)$
    obstaja enoličen morfizem $\sq{\pi}\colon P \to N$. \\[2ex]

    Za funkcijo \sq{\pi} pravimo, da je definirana \alert{korekurzivno}.
  \end{definicija}
\end{frame}

\begin{frame}{Princip rekurzije in korekurzije -- primer}
  Definirajmo funkcijo $\len$ na \alert{seznamih}:
  \begin{align*}
    \len(\nil()) &= 0 \\
    \len(\cons(a, \ell)) &= 1 + \len(\ell)
  \end{align*}
  \[ \len = \ro{[\zero, \succ\circ\prr]} \] \pause
  Definirajmo funkcijo $\func{iterate}(f)$ za \alert{tokove}: \\[-2ex]
  \[ \func{iterate}(f)(a) = (a, f(a), f(f(a)), \ldots) \]
  Korekurzivna definicija:
  \begin{align*}
    \head(\func{iterate}(f)(a)) &= a \\
    \tail(\func{iterate}(f)(a)) &= \func{iterate}(f)(f(a))
  \end{align*}
  \[ \func{iterate}(f) = \sq{\langle \id{A}, f \rangle} \]
\end{frame}

% \begin{frame}{Princip rekurzije in korekurzije -- primer}
%   Definirajmo funkcijo $\map(f)$. \\
%   Na \alert{seznamih}: \\[-2ex]
%   \[ \map(f)([a, b, c]) = [f(a), f(b), f(c)] \]
%   Rekurzivna definicija:
%   \begin{align*}
%     \map(f)(\nil()) &= \nil() \\
%     \map(f)(\cons(a, \ell)) &= \cons(f(a), \map(f)(\ell))
%   \end{align*}
%   Na \alert{tokovih}: \\[-2ex]
%   \[ \map(f)((s_0, s_1, s_2, \ldots)) = (f(s_0), f(s_1), f(s_2), \ldots) \]
%   Korekurzivna definicija:
%   \begin{align*}
%     \head(\map(f)(s)) &= f(\head(s)) \\
%     \tail(\map(f)(s)) &= \map(f)(\tail(s))
%   \end{align*}
% \end{frame}

% \begin{frame}{Princip rekurzije in korekurzije -- primer}
%   Seznami: \\[-7ex]
%   \begin{align*}
%     \map(f)(\nil()) &= \nil() \\
%     \map(f)(\cons(a, \ell)) &= \cons(f(a), \map(f)(\ell))
%   \end{align*}
%   \[ \map(f) = \ro{[\nil(), \cons \circ (f \times \id{L_B})]} \]
%   \[
%     \xymatrix{
%       1 + A\times L_A \ar[r]^(.60){\fnc} \ar@{-->}[d]_{\id{1}+\id{A}\times\map(f)} & L_A \ar@{-->}[d]_{\map(f)} \\
%       1 + A\times L_B \ar[r]^(.60){?} & L_B
%     }
%     \qquad
%     \xymatrix{
%       S_A \ar[r]^(.40){?} \ar@{-->}[d]_{\map(f)} & B \times S_A \ar@{-->}[d]^{\id{B} \times \map(f)} \\
%       S_B \ar[r]^(.40){\fht} & B \times S_B \\
%     }
%   \]
%   Tokovi: \\[-7ex]
%   \begin{align*}
%     \head(\map(f)(s)) &= f(\head(s)) \\
%     \tail(\map(f)(s)) &= \map(f)(\tail(s))
%   \end{align*}
%   \[ \map(f) = \sq{\langle f \circ \head, \tail \rangle} \]
% \end{frame}

\begin{frame}{Princip indukcije}
  Princip indukcije sledi iz minimalnosti začetnih $F$-algeber. \\
  \begin{izrek}
  Naj bo $(M, \mu)$ začetna. Za vsako podalgebro
  \[ i \colon (C, \varphi) \hookrightarrow (M, \mu) \]
  je $i$ izomorfizem.
  \end{izrek}
  Dokaz: \emph{Vsak mono, ki slika v začetni objekt, je izo.}

  \pause

  Kaj to pomeni za \N?

  Množica $A \subseteq \N$ je podalgebra, če je zaprta za $\zero$
  in $\succ$.

  \[
    \left.\begin{matrix}
      0 \in A \\
      n \in A \implies n+1 \in A
    \end{matrix}\right\} \implies A = \N
  \]
\end{frame}

\begin{frame}{Kongruence in bisimulacije}
  \alert{Kongruenca} med $(C, \varphi)$ in $(D, \psi)$ je relacija $R \subseteq
  C \times D$, za katero obstaja preslikava $\rho\colon F(R) \to R$, tako da
  komutira diagram:
  \[ \xymatrix{
    F(C) \ar[d]_\varphi & \ar[l]_{F(\prl)} F(R) \ar[d]^\rho \ar[r]^{F(\prr)} &
    F(D) \ar[d]^\psi  \\ C & \ar[l]_\prl R \ar[r]^\prr & D
  } \]
  \alert{Bisimulacija} med $(P, \pi)$ in $(Q, \vartheta)$ je relacija $R
  \subseteq P \times Q$, za katero obstaja preslikava $\rho\colon R \to F(R)$,
  tako da komutira diagram:
  \[
    \xymatrix{
      P \ar[d]_\pi & \ar[l]_\prl R \ar[r]^\prr \ar[d]^\rho & Q \ar[d]^\vartheta \\
      F(P) & \ar[l]_{F(\prl)} F(R) \ar[r]^{F(\prr)} & F(Q)
    }
  \]
\end{frame}

\begin{frame}{Kongruence in bisimulacije -- primer}
  Relacija $R$ je \alert{kongruenca} na $(\N, [\zero, \succ])$, če velja:
    \begin{gather*}
      (\zero(), \zero()) \in R \\
      (m, n) \in R \implies (\succ(m), \succ(n)) \in R
    \end{gather*}

    \vspace{3ex}

    Relacija $R$ je \alert{bisimulacija} na $(S, \langle \head, \tail \rangle)$, če velja:
    \[ (s, t) \in R \implies
      \begin{cases}
        \head(s) = \head(t) \\
        (\tail(s), \tail(t)) \in R
    \end{cases} \]
\end{frame}

% \begin{frame}{Indukcija in koindukcija}
%   \begin{trditev}[Princip indukcije]
%     Naj bo $(M, \mu)$ začetna algebra. Potem za vsako kongruenco $R$ na $M$ in vsak $m \in M$ velja
%     \[ (m, m) \in R\,. \]
%   \end{trditev}
%   \begin{trditev}[Princip koindukcije]
%     \label{izr:bisim}
%     Naj bo $(N, \nu)$ končna koalgebra. Potem za vsako bisimulacijo $R$ na $N$ in za
%     vsaka $n, n' \in N$ velja: Če je
%     \[ (n, n') \in R, \]
%     potem velja
%     \[ n = n'. \]
%   \end{trditev}
% \end{frame}

\begin{frame}{Indukcija in koindukcija}
  \alert{Princip indukcije} za začetne algebre $(M, \mu)$:
  \[ \text{za vsako kongruenco $R \subseteq M \times M$ velja $\Delta_M \subseteq R$}. \]

  \vspace{4ex}

  \alert{Princip koindukcije} za končne koalgebre $(N, \nu)$:
  \[ \text{za vsako bisimulacijo $R \subseteq N \times N$ velja $R \subseteq \Delta_N$}. \]
\end{frame}


\end{document}

% vim: spell spelllang=sl
% vim: foldlevel=99
